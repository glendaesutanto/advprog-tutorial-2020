package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {
        // ToDo: Complete me
        AttackBehavior attackWithGun = new AttackWithGun();
        DefenseBehavior defendWithBarrier = new DefendWithBarrier();

        public String getAlias() {
                return "Agile Adventurer";
        }

        public AgileAdventurer() {
                setAttackBehavior(attackWithGun);
                setDefenseBehavior(defendWithBarrier);
        }
}
