package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {
        // ToDo: Complete me
        AttackBehavior attackWithSword = new AttackWithSword();
        DefenseBehavior defendWithArmor = new DefendWithArmor();

        public String getAlias() {
                return "Knight Adventurer";
        }

        public KnightAdventurer() {
                setAttackBehavior(attackWithSword);
                setDefenseBehavior(defendWithArmor);
        }
}
