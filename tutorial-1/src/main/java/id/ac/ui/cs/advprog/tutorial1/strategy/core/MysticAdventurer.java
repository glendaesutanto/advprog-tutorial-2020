package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {
        // ToDo: Complete me
        AttackBehavior attackWithMagic = new AttackWithMagic();
        DefenseBehavior defendWithShield = new DefendWithShield();

        public String getAlias() {
                return "Mystic Adventurer";
        }

        public MysticAdventurer() {
                setAttackBehavior(attackWithMagic);
                setDefenseBehavior(defendWithShield);
        }
}
