package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.*;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    private ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    public void cast() {
        for (int i = 0; i < this.spells.size(); i++) {
            this.spells.get(i).cast();
        }
    }

    public void undo() {
        for (int i = this.spells.size() - 1; i >= 0; i--) {
            this.spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
