package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;

import java.util.Random;

public class RegularUpgrade extends Weapon {

    Weapon weapon;

    public RegularUpgrade(Weapon weapon) {

        this.weapon = weapon;
    }

    @Override
    public String getName() {
        return weapon.getName();
    }

    // Senjata bisa dienhance hingga 1-5 ++
    @Override
    public int getWeaponValue() {
        // TODO: Complete me
        Random rand = new Random();
        int value_upgraded = rand.nextInt(5) + 1;
        return weapon.getWeaponValue() + value_upgraded;
    }

    @Override
    public String getDescription() {
        // TODO: Complete me
        return weapon.getDescription() + " -> Regular Upgrade";
    }
}
