package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Sword;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RegularUpgradeTest {

    private RegularUpgrade regularUpgrade;

    @BeforeEach
    public void setUp() {
        regularUpgrade = new RegularUpgrade(new Sword());
    }

    @Test
    public void testMethodGetWeaponName() {
        // TODO: Complete me
        assertEquals("Sword", regularUpgrade.getName());
    }

    @Test
    public void testMethodGetWeaponDescription() {
        // TODO: Complete me
        assertTrue(regularUpgrade.getDescription().substring(regularUpgrade.getDescription().length() - 15)
                .equals("Regular Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue() {
        // TODO: Complete me
        boolean dummy;
        int weaponValue = regularUpgrade.getWeaponValue();
        dummy = (weaponValue == regularUpgrade.weapon.getWeaponValue() + 1);
        dummy = dummy || (weaponValue == regularUpgrade.weapon.getWeaponValue() + 2);
        dummy = dummy || (weaponValue == regularUpgrade.weapon.getWeaponValue() + 3);
        dummy = dummy || (weaponValue == regularUpgrade.weapon.getWeaponValue() + 4);
        dummy = dummy || (weaponValue == regularUpgrade.weapon.getWeaponValue() + 5);
        assertTrue(dummy);
    }
}
