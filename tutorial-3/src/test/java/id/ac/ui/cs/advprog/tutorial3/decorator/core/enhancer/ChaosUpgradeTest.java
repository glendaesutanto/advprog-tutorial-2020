package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Gun;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ChaosUpgradeTest {

    private ChaosUpgrade chaosUpgrade;

    @BeforeEach
    public void setUp() {
        chaosUpgrade = new ChaosUpgrade(new Gun());
    }

    @Test
    public void testMethodGetWeaponName() {
        // TODO: Complete me
        assertEquals("Gun", chaosUpgrade.getName());
    }

    @Test
    public void testGetMethodWeaponDescription() {
        // TODO: Complete me
        assertTrue(chaosUpgrade.getDescription().substring(chaosUpgrade.getDescription().length() - 13)
                .equals("Chaos Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue() {
        // TODO: Complete me
        boolean dummy;
        int weaponValue = chaosUpgrade.getWeaponValue();
        dummy = (weaponValue == chaosUpgrade.weapon.getWeaponValue() + 50);
        dummy = dummy || (weaponValue == chaosUpgrade.weapon.getWeaponValue() + 51);
        dummy = dummy || (weaponValue == chaosUpgrade.weapon.getWeaponValue() + 52);
        dummy = dummy || (weaponValue == chaosUpgrade.weapon.getWeaponValue() + 53);
        dummy = dummy || (weaponValue == chaosUpgrade.weapon.getWeaponValue() + 54);
        dummy = dummy || (weaponValue == chaosUpgrade.weapon.getWeaponValue() + 55);
        assertTrue(dummy);
    }

}
