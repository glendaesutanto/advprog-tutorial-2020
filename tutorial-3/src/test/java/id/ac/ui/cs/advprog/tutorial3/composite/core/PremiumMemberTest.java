package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Aqua", "Goddess");
    }

    @Test
    public void testMethodGetName() {
        // TODO: Complete me
        assertEquals("Aqua", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        // TODO: Complete me
        assertEquals("Goddess", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        // TODO: Complete me
        int sizeBefore = member.getChildMembers().size();
        Member dummy = new PremiumMember("dummyName", "dummyRole");
        member.addChildMember(dummy);
        assertEquals(member.getChildMembers().size(), sizeBefore + 1);
    }

    @Test
    public void testMethodRemoveChildMember() {
        // TODO: Complete me
        Member dummy = new PremiumMember("dummyName", "dummyRole");
        member.addChildMember(dummy);
        int sizeBefore = member.getChildMembers().size();
        member.removeChildMember(dummy);
        assertEquals(member.getChildMembers().size(), sizeBefore - 1);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        // TODO: Complete me
        Member dummy1 = new PremiumMember("dummyName_1", "dummyRole_1");
        Member dummy2 = new PremiumMember("dummyName_2", "dummyRole_2");
        Member dummy3 = new PremiumMember("dummyName_3", "dummyRole_3");
        member.addChildMember(dummy1);
        member.addChildMember(dummy2);
        member.addChildMember(dummy3);
        int sizeBefore = member.getChildMembers().size();
        Member dummy4 = new PremiumMember("dummyName_4", "dummyRole_4");
        member.addChildMember(dummy4);
        assertEquals(member.getChildMembers().size(), sizeBefore);
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        // TODO: Complete me
        Member guild_master = new PremiumMember("dummyMaster", "Master");
        Member dummy1 = new PremiumMember("dummyName_1", "dummyRole_1");
        Member dummy2 = new PremiumMember("dummyName_2", "dummyRole_2");
        Member dummy3 = new PremiumMember("dummyName_3", "dummyRole_3");
        guild_master.addChildMember(dummy1);
        guild_master.addChildMember(dummy2);
        guild_master.addChildMember(dummy3);
        int sizeBefore = guild_master.getChildMembers().size();
        Member dummy4 = new PremiumMember("dummyName_4", "dummyRole_4");
        guild_master.addChildMember(dummy4);
        assertEquals(guild_master.getChildMembers().size(), sizeBefore + 1);
    }
}
