package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RawUpgradeTest {

    private RawUpgrade rawUpgrade;

    @BeforeEach
    public void setUp() {
        rawUpgrade = new RawUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName() {
        // TODO: Complete me
        assertEquals("Shield", rawUpgrade.getName());
    }

    @Test
    public void testMethodGetDescription() {
        // TODO: Complete me
        assertTrue(
                rawUpgrade.getDescription().substring(rawUpgrade.getDescription().length() - 11).equals("Raw Upgrade"));
    }

    @Test
    public void testMethodGetWeaponValue() {
        // TODO: Complete me
        boolean dummy;
        int weaponValue = rawUpgrade.getWeaponValue();
        dummy = (weaponValue == rawUpgrade.weapon.getWeaponValue() + 5);
        dummy = dummy || (weaponValue == rawUpgrade.weapon.getWeaponValue() + 6);
        dummy = dummy || (weaponValue == rawUpgrade.weapon.getWeaponValue() + 7);
        dummy = dummy || (weaponValue == rawUpgrade.weapon.getWeaponValue() + 8);
        dummy = dummy || (weaponValue == rawUpgrade.weapon.getWeaponValue() + 9);
        dummy = dummy || (weaponValue == rawUpgrade.weapon.getWeaponValue() + 10);
        assertTrue(dummy);
    }
}
